working_directory "/home/__APP_NAME__/pennstation/current"
pid "/home/__APP_NAME__/pennstation/current/tmp/pids/unicorn.pid"
stderr_path "/home/__APP_NAME__/pennstation/current/log/unicorn.log"
stdout_path "/home/__APP_NAME__/pennstation/current/log/unicorn.log"

listen "/home/__APP_NAME__/pennstation/shared/tmp/unicorn.sock"
worker_processes 2
timeout 30

before_fork do |server, worker|
  # the following is highly recomended for Rails + "preload_app true"
  # as there's no need for the master process to hold a connection

  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.connection.disconnect!

  ##
  # When sent a USR2, Unicorn will suffix its pidfile with .oldbin and
  # immediately start loading up a new version of itself (loaded with a new
  # version of our app). When this new Unicorn is completely loaded
  # it will begin spawning workers. The first worker spawned will check to
  # see if an .oldbin pidfile exists. If so, this means we've just booted up
  # a new Unicorn and need to tell the old one that it can now die. To do so
  # we send it a QUIT.
  #
  # Using this method we get 0 downtime deploys.

  old_pid = '/home/__APP_NAME__/pennstation/current/tmp/pids/unicorn.pid.oldbin'
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end
end

after_fork do |server, worker|
    # Here we are establishing the connection after forking worker
    # processes

  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.establish_connection
end
